package dev.codenmore.tilegame.worlds;

import dev.codenmore.tilegame.Handler;
import dev.codenmore.tilegame.tiles.Tile;
import dev.codenmore.tilegame.utils.Utils;

import java.awt.*;

public class World {

    private final Handler handler;
    private int width, height;
    private int spawnX, spawnY;
    private int[][] worldTiles;

    public World(Handler handler, String path){
        this.handler = handler;
        loadWorld(path);
    }

    public void tick() {
    }

    public void render(Graphics graphics){

        // The xStart / yStart int calculates if a Tile is out of sight.
        // The xEnd / yEnd int calculates if a Tile is out of sight. you have to render one further therefore +1 for now
        // thus improving the rendering speed. (not the whole world is rendered every time)
        int xStart = (int) Math.max(0, handler.getGameCamera().getxOffset() / Tile.TILE_WIDTH);
        int xEnd = (int) Math.min(width, (handler.getGameCamera().getxOffset() + handler.getWidth()) / Tile.TILE_WIDTH + 1);

        int yStart = (int) Math.max(0, handler.getGameCamera().getyOffset() / Tile.TILE_HEIGHT);
        int yEnd = (int) Math.min(height, (handler.getGameCamera().getyOffset() + handler.getHeight()) / Tile.TILE_HEIGHT + 1);

        for (int y = yStart; y < yEnd; y++) {
            for (int x = xStart; x < xEnd; x++) {
                getTile(x, y).render(graphics,
                        (int) (x * Tile.TILE_WIDTH - handler.getGameCamera().getxOffset()),
                        (int) (y * Tile.TILE_HEIGHT - handler.getGameCamera().getyOffset()));
            }
        }

    }

    public Tile getTile(int x, int y) {

        // if player somehow gets out of the map.
        if (x < 0 || y < 0 || x > width || y > height) {
            return Tile.grassTile;
        }
        // Returning Tile =  1.TileClass
        // 2.look into the saved tiles and
        // 3.give back the Tile that value is at world [x][y]

        Tile currTile = Tile.tiles[worldTiles[x][y]];
        if (currTile != null) {
            return currTile;
        } else {
            return Tile.dirtTile;
        }
    }

    private void loadWorld(String path){
        String file = Utils.loadFileAsString(path);
        String[] tokens = file.split("\\s+");
        width = Utils.parseInt(tokens[0]);
        height = Utils.parseInt(tokens[1]);
        spawnX = Utils.parseInt(tokens[2]);
        spawnY = Utils.parseInt(tokens[3]);

        worldTiles = new int[width][height];

        int pos = 4;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                worldTiles[x][y] = Utils.parseInt(tokens[pos]);     // (x+y * width) + 4
                pos++;
            }
        }
    }

    // Getters
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
