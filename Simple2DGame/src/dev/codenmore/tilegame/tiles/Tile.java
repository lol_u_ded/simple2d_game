package dev.codenmore.tilegame.tiles;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {

    // World
    public static Tile[] tiles = new Tile[256];
    public static Tile grassTile = new GrassTile(0);
    public static Tile dirtTile = new DirtTile(1);
    public static Tile sandTile = new SandTile(2);
    public static Tile stoneWall = new StoneWallTile(3);
    public static Tile sandWall = new SandWallTile(4);

    // Class

    public static final int TILE_WIDTH = 64;
    public static final int TILE_HEIGHT = 64;

    protected BufferedImage texture;
    protected final int id;

    public Tile(BufferedImage texture, int id){
        this.texture = texture;
        this.id = id;
        tiles[id] = this;
    }

    public int getId() {
        return id;
    }

    public boolean isSolid(){
        return false;
    }

    public void tick(){}

    public void render(Graphics graphics, int x, int y ){
        graphics.drawImage(texture, x, y, TILE_WIDTH, TILE_HEIGHT, null);
    }
}
