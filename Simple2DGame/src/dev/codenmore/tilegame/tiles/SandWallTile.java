package dev.codenmore.tilegame.tiles;

import dev.codenmore.tilegame.gfx.Assets;

public class SandWallTile extends Tile{

    public SandWallTile(int id) {
        super(Assets.sandWallH1, id);
    }

    @Override
    public boolean isSolid(){
        return true;
    }

}
