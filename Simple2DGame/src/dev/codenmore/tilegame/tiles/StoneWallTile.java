package dev.codenmore.tilegame.tiles;

import dev.codenmore.tilegame.gfx.Assets;

import java.awt.image.BufferedImage;

public class StoneWallTile extends Tile{

    public StoneWallTile(int id) {
        super(Assets.stoneWallH1, id);
    }

    @Override
    public boolean isSolid(){
        return true;
    }

}
