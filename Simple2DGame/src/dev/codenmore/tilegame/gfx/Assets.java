package dev.codenmore.tilegame.gfx;

import java.awt.image.BufferedImage;

public class Assets {

    // sets the Sprite Sheet texture size
    private static final int WIDTH = 64;
    private static final int HEIGHT = 64;
    private static final int JUMP = 64;

    // textures
    public static BufferedImage playerFIdle, playerFWalk1, playerFWalk2, playerFWalk3;
    public static BufferedImage playerLIdle, playerLWalk1, playerLWalk2, playerLWalk3;
    public static BufferedImage playerRIdle, playerRWalk1, playerRWalk2, playerRWalk3;
    public static BufferedImage playerUIdle, playerUWalk1, playerUWalk2, playerUWalk3;

    public static BufferedImage stoneWallH1, sandH1, grassH1, dirtH1, sandWallH1;


    public static void init(){

        //Player Sprites
        SpriteSheet player = new SpriteSheet(ImageLoader.loadImage("/textures/player/player.png"));
        playerFIdle = player.crop(0,0,WIDTH,HEIGHT);
        playerFWalk1 = player.crop(JUMP,0,WIDTH,HEIGHT);
        playerFWalk2 = player.crop(2*JUMP,0,WIDTH,HEIGHT);
        playerFWalk3 = player.crop(3*JUMP,0,WIDTH,HEIGHT);

        playerLIdle = player.crop(0,JUMP,WIDTH,HEIGHT);
        playerLWalk1 = player.crop(JUMP,JUMP,WIDTH,HEIGHT);
        playerLWalk2 = player.crop(2*JUMP,JUMP,WIDTH,HEIGHT);
        playerLWalk3 = player.crop(3*JUMP,JUMP,WIDTH,HEIGHT);

        playerRIdle = player.crop(0,JUMP*2,WIDTH,HEIGHT);
        playerRWalk1 = player.crop(JUMP,JUMP*2,WIDTH,HEIGHT);
        playerRWalk2 = player.crop(2*JUMP,JUMP*2,WIDTH,HEIGHT);
        playerRWalk3 = player.crop(3*JUMP,JUMP*2,WIDTH,HEIGHT);

        playerUIdle = player.crop(0,JUMP*3,WIDTH,HEIGHT);
        playerUWalk1 = player.crop(JUMP,JUMP*3,WIDTH,HEIGHT);
        playerUWalk2 = player.crop(2*JUMP,JUMP*3,WIDTH,HEIGHT);
        playerUWalk3 = player.crop(3*JUMP,JUMP*3,WIDTH,HEIGHT);

        SpriteSheet ground = new SpriteSheet(ImageLoader.loadImage("/textures/objects/ground.png"));
        stoneWallH1 = ground.crop(0,0,WIDTH,HEIGHT);
        sandH1 = ground.crop(0,JUMP,WIDTH,HEIGHT);
        grassH1 = ground.crop(0,JUMP*2,WIDTH,HEIGHT);
        dirtH1 = ground.crop(0,JUMP*3,WIDTH,HEIGHT);
        sandWallH1 = ground.crop(0,JUMP*4,WIDTH,HEIGHT);

    }
}
