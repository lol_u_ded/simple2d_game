package dev.codenmore.tilegame.entities.creatures;

import dev.codenmore.tilegame.Handler;
import dev.codenmore.tilegame.gfx.Assets;

import java.awt.*;

public class Player extends Creature{

    public Player(Handler handler, float x, float y) {
        super(handler, x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
        bounds.x = 20;
        bounds.y = 34;
        bounds.width = 23;
        bounds.height = 27;
    }

    // the movement from pressed Keys is added,
    // after that the move method (creatures) will execute movement.
    @Override
    public void tick() {
        getInput();
        move();
        handler.getGameCamera().centerOnEntity(this);
    }

    // unique for PlayerClass
    private void getInput(){
        setxMove(0);setyMove(0);

        if(handler.getKeyManager().up){
            setyMove(-speed);
        }
        if(handler.getKeyManager().down){
            setyMove(speed);
        }
        if(handler.getKeyManager().left) {
            setxMove(-speed);
        }
        if(handler.getKeyManager().right){
            setxMove(speed);
        }
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Assets.playerFIdle,
                (int) (x - handler.getGameCamera().getxOffset()),
                (int) (y - handler.getGameCamera().getyOffset()),
                width, height, null);

        // show collision box
        if (COLLISION_BOX) {
            graphics.setColor(Color.RED);
            graphics.fillRect(
                    ((int) (x + bounds.x - handler.getGameCamera().getxOffset())),
                    ((int) (y + bounds.y - handler.getGameCamera().getyOffset())),
                    bounds.width,
                    bounds.height);
        }
    }
}
