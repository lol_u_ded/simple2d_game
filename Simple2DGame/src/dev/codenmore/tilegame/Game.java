package dev.codenmore.tilegame;

import dev.codenmore.tilegame.display.Display;
import dev.codenmore.tilegame.gfx.Assets;
import dev.codenmore.tilegame.gfx.GameCamera;
import dev.codenmore.tilegame.input.KeyManager;
import dev.codenmore.tilegame.states.*;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Game implements Runnable{

    private final int FPS = 60;
    private final double NANOSECONDS_PER_SECOND = 1000000000;

    private Display display;

    private final int width;
    private final int height;
    public final String title;

    private boolean running = false;
    private Thread thread;

    private BufferStrategy bufferStrategy;
    private Graphics graphics;

    //States
    private State gameState;
    private State menuState;
    private State settingsState;

    //Input
    private final KeyManager keyManager;

    //Camera
    private GameCamera gameCamera;

    //Handler
    private Handler handler;

    public Game(String title, int width, int height){
        this.title = title;
        this.width = width;
        this.height = height;
        this.keyManager = new KeyManager();
    }

    // Initial start method
    private void init() {
        display = new Display(title, width, height);
        display.getFrame().addKeyListener(keyManager);
        Assets.init();
        handler = new Handler(this);

        gameCamera = new GameCamera(handler, 0, 0);


        gameState = new GameState(handler);
        menuState = new MenuState(handler);
        settingsState = new SettingsState(handler);
        GameStateManager.setCurrentState(gameState);
    }

    // Getter GameCamera
    public GameCamera getGameCamera(){
        return gameCamera;
    }

    // Getter Width
    public int getWidth() {
        return width;
    }

    // Getter Height
    public int getHeight() {
        return height;
    }

    //todo: updates variables
    private void tick(){
        keyManager.tick();
        if(GameStateManager.getCurrentState() != null){
            GameStateManager.getCurrentState().tick();
        }
    }

    // Draw something according to tick method.
    private void render(){
        // BufferStrategy tells how something is drawn to the screen, uses Buffers ("hidden Windows")
        bufferStrategy = display.getCanvas().getBufferStrategy();
        if(bufferStrategy == null){
            display.getCanvas().createBufferStrategy(2);
            return;
        }
        graphics = bufferStrategy.getDrawGraphics();
        //Clear Screen
        graphics.clearRect(0,0,width,height);

        // Draw here

        if(GameStateManager.getCurrentState() != null){
            GameStateManager.getCurrentState().render(graphics);
        }

        // End drawing

        bufferStrategy.show();
        graphics.dispose();
    }

    // the Game Loop
    public void run(){

        double timePerTick = NANOSECONDS_PER_SECOND / FPS;
        double delta =0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        int ticks = 0;

        init();

        // GameLoop
        while(running){
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;
            if(delta >= 1) {
                tick();
                render();
                ticks++;
                delta--;
            }
            if(timer >= NANOSECONDS_PER_SECOND ){
                System.out.println("FPS=" + ticks );
                ticks = 0; timer = 0;
            }
        }
        stop();
    }

    public KeyManager getKeyManager(){
        return keyManager;
    }

    // Start the Thread
    public synchronized void start(){
        //just start if game isn't initialised jet
        if(!running) {
            running = true;
            thread = new Thread(this);          // implements Game Class
            thread.start();
        }
    }

    // Stop the Thread
    public synchronized void stop(){
        //just stop if game is initialised
        if(running) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
